# app_design_pro

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Credits: [Go](https://www.udemy.com/course/flutter-ios-android-fernando-herrera/)
## Preview

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/8f827b5b-4888-4ec8-b1c8-ae2b882be7e0.jpg" alt="Preview" />